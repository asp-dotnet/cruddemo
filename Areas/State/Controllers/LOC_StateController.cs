﻿using CRUDDemo.Areas.Country.Models;
using CRUDDemo.Areas.State.Models;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using System.Data.SqlClient;

namespace CRUDDemo.Areas.State.Controllers
{
    [Area("State")]
    [Route("State/[controller]/[action]")]

    public class LOC_StateController : Controller
    {
        private readonly IConfiguration Configuration;

        public LOC_StateController(IConfiguration _configuration)
        {
            Configuration = _configuration;
        }

        #region Select All State
        public IActionResult Index()
        {
            DataTable dt = new DataTable();
            string str = Configuration.GetConnectionString("myConnectionString");
            SqlConnection conn = new SqlConnection(str);
            conn.Open();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "PR_State_selectAll";
            SqlDataReader dr = cmd.ExecuteReader();

            if (dr.HasRows)
            {
                dt.Load(dr);
            }
            conn.Close();
            return View("LOC_StateList", dt);
        }
        #endregion

        #region Delete
        [Route("DeleteState")]
        public IActionResult DeleteState(int StateID)
        {
            SqlConnection conn = new SqlConnection(this.Configuration.GetConnectionString("myConnectionString"));
            conn.Open();
            SqlCommand sqlcommand = conn.CreateCommand();
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.CommandText = "PR_State_DeleteByPK";
            sqlcommand.Parameters.Add("@StateID", SqlDbType.Int).Value = StateID;
            if (Convert.ToBoolean(sqlcommand.ExecuteNonQuery()))
            {
                TempData["Message"] = " ";
            }
            conn.Close();
            return RedirectToAction("Index");
        }
        #endregion

        #region select by primaryKey

        public IActionResult AddEdit(int? StateID)
        {
            FillCountryDDL();
            if (StateID != null)
            {
                string str = Configuration.GetConnectionString("myConnectionString");
                SqlConnection conn = new SqlConnection(str);
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "PR_State_SelectByPK";
                cmd.Parameters.Add("@StateID", SqlDbType.Int).Value = StateID;
                DataTable dt = new DataTable();
                SqlDataReader dr = cmd.ExecuteReader();
                LOC_StateModel state = new LOC_StateModel();
                if (dr.Read())
                {


                    state.StateName = dr["StateName"].ToString();
                    state.StateCode = dr["StateCode"].ToString();
                    state.CountryID = Convert.ToInt32(dr["CountryID"]);

                }
                return View("LOC_StateAddEdit", state);
            }
            return View("LOC_StateAddEdit");
        }
        #endregion

        #region Add Edit
        [HttpPost]
        public IActionResult Save(LOC_StateModel model_LOC_State)
        {
            if (ModelState.IsValid)
            {
                SqlConnection objConn = new
                SqlConnection(this.Configuration.GetConnectionString("myConnectionString"));
                objConn.Open();
                SqlCommand objCmd = objConn.CreateCommand();
                objCmd.CommandType = CommandType.StoredProcedure;
                if (model_LOC_State.StateID == null)
                {
                    objCmd.CommandText = "PR_State_Insert";
                    objCmd.Parameters.AddWithValue("@Created", DateTimeOffset.Now);
                }
                else
                {
                    objCmd.CommandText = "PR_State_UpdateByPK";
                    objCmd.Parameters.AddWithValue("@StateID", model_LOC_State.StateID);
                }
                objCmd.Parameters.AddWithValue("@StateName", model_LOC_State.StateName);
                objCmd.Parameters.AddWithValue("@StateCode", model_LOC_State.StateCode);
                objCmd.Parameters.AddWithValue("@CountryID", model_LOC_State.CountryID);

                if (Convert.ToBoolean(objCmd.ExecuteNonQuery()))
                {
                    if (model_LOC_State.StateID == null)
                        TempData["StateInsertMsg"] = " ";
                    else
                    {
                        TempData["StateInsertMsg"] = " ";
                        return RedirectToAction("Index");
                    }
                }
                objConn.Close();
            }
            return RedirectToAction("Index");
        }

        #endregion


        #region fillCountryDDL
        public void FillCountryDDL()
        {
            List<LOC_CountryDropDownModel> list = new List<LOC_CountryDropDownModel>();
            SqlConnection con = new SqlConnection(this.Configuration.GetConnectionString("myConnectionString"));
            con.Open();
            SqlCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "PR_Country_SelectAll";
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    LOC_CountryDropDownModel countryDropDown = new LOC_CountryDropDownModel()
                    {
                        CountryID = Convert.ToInt32(dr["CountryID"]),
                        CountryName = dr["CountryName"].ToString()
                    };
                    list.Add(countryDropDown);
                }
                dr.Close();
            }
            con.Close();
            ViewBag.CountryList = list;
        }
        #endregion


    }
}

