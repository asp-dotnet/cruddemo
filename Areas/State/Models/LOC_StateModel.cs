﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace CRUDDemo.Areas.State.Models
{
    public class LOC_StateModel
    {
        public int? StateID { get; set; }
        [Required(ErrorMessage = "State Name is Required")]
        [DisplayName("State Name")]
        public string StateName { get; set; }
        [Required(ErrorMessage = "Please Select Country")]
        [DisplayName("Country Name")]
        public int CountryID { get; set; }
        [Required(ErrorMessage = "State Code is Required")]
        [DisplayName("State Code")]
        [StringLength(maximumLength: 100, MinimumLength = 1)]
        public string StateCode { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime Modified { get; set; }
    }

    public class StateDropDownModel
    {
        public int? StateID { get; set; }
        public string? StateName { get; set; }
    }
}
