﻿using CRUDDemo.Areas.Country.Models;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using System.Data.SqlClient;

namespace CRUDDemo.Areas.Country.Controllers
{
    [Area("Country")]
    [Route("Country/[controller]/[action]")]

    public class LOC_CountryController : Controller
    {
        private readonly IConfiguration Configuration;

        public LOC_CountryController(IConfiguration _configuration)
        {
            Configuration = _configuration;
        }

        #region Select All
        public IActionResult Index()
        {
            string str = this.Configuration.GetConnectionString("myConnectionString");

            SqlConnection conn = new SqlConnection(str);
            conn.Open();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "PR_Country_SelectAll";

            SqlDataReader dr = cmd.ExecuteReader();
            DataTable dt = new DataTable();

            if (dr.HasRows)
            {
                dt.Load(dr);
            }

            conn.Close();

            return View("LOC_CountryList", dt);
        }
        #endregion

        #region Delete
        public IActionResult Delete(int CountryID)
        {
            string str = this.Configuration.GetConnectionString("myConnectionString");
            SqlConnection conn = new SqlConnection(str);
            conn.Open();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "PR_Country_DeleteByPK";
            cmd.Parameters.Add("@CountryID", SqlDbType.Int).Value = CountryID;
            cmd.ExecuteNonQuery();
            conn.Close();

            return RedirectToAction("Index");
        }
        #endregion

        #region Update
        public IActionResult AddEdit(int? CountryID)
        {
            if (CountryID != null)
            {
                string str = this.Configuration.GetConnectionString("myConnectionString");
                SqlConnection objConn = new SqlConnection(str);
                objConn.Open();
                SqlCommand objCmd = objConn.CreateCommand();
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.CommandText = "PR_Country_SelectByPK";
                objCmd.Parameters.Add("@CountryID", SqlDbType.Int).Value = CountryID;
                SqlDataReader dr = objCmd.ExecuteReader();
                LOC_CountryModel country = new LOC_CountryModel();

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        country.CountryName = dr["CountryName"].ToString();
                        country.CountryCode = dr["CountryCode"].ToString();
                    }
                }
                return View("LOC_CountryAddEdit", country);
            }
            return View("LOC_CountryAddEdit");
        }
        #endregion

        #region Add Edit
        [HttpPost]
        public IActionResult Save(LOC_CountryModel modelLOC_Country)
        {
            string str = Configuration.GetConnectionString("myConnectionString");
            SqlConnection conn = new SqlConnection(str);
            conn.Open();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;

            if (modelLOC_Country.CountryID == null)
            {
                cmd.CommandText = "PR_Country_Insert";
                cmd.Parameters.AddWithValue("@Created", DateTimeOffset.Now);
            }
            else
            {
                cmd.CommandText = "PR_Country_UpdateByPK";
                cmd.Parameters.Add("@CountryID", SqlDbType.Int).Value = modelLOC_Country.CountryID;
            }
            cmd.Parameters.Add("@CountryName", SqlDbType.VarChar).Value = modelLOC_Country.CountryName;
            cmd.Parameters.Add("@CountryCode", SqlDbType.VarChar).Value = modelLOC_Country.CountryCode;

            if (Convert.ToBoolean(cmd.ExecuteNonQuery()))
            {
                if (modelLOC_Country.CountryID == null)
                {
                    TempData["CountryInsertMsg"] = "";
                }
                else
                {
                    TempData["CountryInsertMsg"] = "";
                    return RedirectToAction("Index");
                }
            }
            conn.Close();

            return RedirectToAction("Index");
        }
        #endregion

    }
}
