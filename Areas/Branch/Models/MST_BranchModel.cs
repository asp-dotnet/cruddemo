﻿using System.ComponentModel.DataAnnotations;

namespace CRUDDemo.Areas.Branch.Models
{
    public class MST_BranchModel
    {
        public int? BranchID { get; set; }

        [Required(ErrorMessage = "Enter Branch Name")]
        public string? BranchName { get; set; }

        [Required(ErrorMessage = "Enter Branch Code")]
        public string? BranchCode { get; set; }

        public DateTime? Created { get; set; }

        public DateTime? Modified { get; set; }

    }

    public class BranchDropDownModel
    {
        public int? BranchID { get; set; }
        public string? BranchName { get; set; }
    }
}
