﻿using CRUDDemo.Areas.Branch.Models;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using System.Data.SqlClient;

namespace CRUDDemo.Areas.Branch.Controllers
{
    [Area("Branch")]
    [Route("MST_Branch/[controller]/[action]")]
    public class MST_BranchController : Controller
    {
        private IConfiguration Configuration;
        public MST_BranchController(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        #region Select All

        public IActionResult Index()
        {
            DataTable dt = new DataTable();
            string str = Configuration.GetConnectionString("myConnectionString");
            SqlConnection conn = new SqlConnection(str);
            conn.Open();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "PR_Mst_Branch_SelectAll";
            SqlDataReader dr = cmd.ExecuteReader();

            if (dr.HasRows)
            {
                dt.Load(dr);
            }
            return View("MST_BranchList", dt);
        }
        #endregion

        #region SelectByPK
        public ActionResult Add(int? BranchID)
        {
            if (BranchID != null)
            {
                string str = Configuration.GetConnectionString("myConnectionString");
                SqlConnection conn = new SqlConnection(str);
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "PR_Mst_Branch_SelectByPK";

                cmd.Parameters.Add("@BranchID", SqlDbType.Int).Value = BranchID;
                DataTable dt = new DataTable();
                SqlDataReader dr = cmd.ExecuteReader();
                MST_BranchModel branch = new MST_BranchModel();
                if (dr.Read())
                {


                    branch.BranchName = dr["BranchName"].ToString();
                    branch.BranchCode = dr["BranchCode"].ToString();

                }
                return View("MST_BranchAddEdit", branch);
            }
            return View("MST_BranchAddEdit");
        }


        #endregion

        #region Add Edit
        [HttpPost]
        public IActionResult Save(MST_BranchModel modelMST_Branch)
        {
            string str = Configuration.GetConnectionString("myConnectionString");
            SqlConnection conn = new SqlConnection(str);
            conn.Open();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;

            if (modelMST_Branch.BranchID == null)
            {
                cmd.CommandText = "PR_Mst_Branch_Insert";
                cmd.Parameters.AddWithValue("@Created", DateTimeOffset.Now);
            }
            else
            {
                cmd.CommandText = "PR_Mst_Branch_UpdateByPK";
                cmd.Parameters.Add("@BranchID", SqlDbType.Int).Value = modelMST_Branch.BranchID;
            }
            cmd.Parameters.Add("@BranchName", SqlDbType.VarChar).Value = modelMST_Branch.BranchName;
            cmd.Parameters.Add("@BranchCode", SqlDbType.VarChar).Value = modelMST_Branch.BranchCode;

            if (Convert.ToBoolean(cmd.ExecuteNonQuery()))
            {
                if (modelMST_Branch.BranchID == null)
                {
                    TempData["BranchInsertMsg"] = "";
                }
                else
                {
                    TempData["BranchInsertMsg"] = "";
                    return RedirectToAction("Index");
                }
            }
            conn.Close();

            return RedirectToAction("Index");
        }
        #endregion

        #region Delete
        [Route("DeleteBranch")]
        public IActionResult DeleteBranch(int branchID)
        {
            SqlConnection conn = new SqlConnection(this.Configuration.GetConnectionString("myConnectionString"));
            conn.Open();
            SqlCommand sqlcommand = conn.CreateCommand();
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.CommandText = "PR_Mst_Branch_DeleteByPK";
            sqlcommand.Parameters.Add("@BranchID", SqlDbType.Int).Value = branchID;
            if (Convert.ToBoolean(sqlcommand.ExecuteNonQuery()))
            {
                TempData["Message"] = " ";
            }
            conn.Close();
            return RedirectToAction("Index");
        }
        #endregion

    }
}