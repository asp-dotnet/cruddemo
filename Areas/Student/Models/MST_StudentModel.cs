﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace CRUDDemo.Areas.Student.Models
{
    public class MST_StudentModel
    {
        public int? StudentID { get; set; }

        public int? BranchID { get; set; }

        public int? CityID { get; set; }

        [Required(ErrorMessage = "Enter Student Name")]
        [DisplayName("Student Name")]
        public string? StudentName { get; set; }

        [Required(ErrorMessage = "Enter Student Mobile Number")]
        [DisplayName("Student Mobille Number")]
        [StringLength(maximumLength: 10)]
        public string? MobileNoStudent { get; set; }

        [Required(ErrorMessage = "Enter Email")]
        [DisplayName("Email Address")]
        [EmailAddress]
        public string? Email { get; set; }

        [Required(ErrorMessage = "Enter Father's/Guardian's Mobile number")]
        [DisplayName("Father's/Guardian's Mobile Number")]
        [StringLength(maximumLength: 10)]
        public string? MobileNoFather { get; set; }

        [Required(ErrorMessage = "Enter Address")]
        public string? Address { get; set; }

        [Required(ErrorMessage = "Enter BirthDate")]
        [DisplayName("Birth Date")]
        [DataType(DataType.Date)]
        public DateTime BirthDate { get; set; }

        public int? Age { get; set; }

        public bool IsActive { get; set; }

        [Required(ErrorMessage = "Select Gender")]
        public string? Gender { get; set; }
        [Required(ErrorMessage = "Enter Password")]

        public string? Password { get; set; }

        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
    }
}
