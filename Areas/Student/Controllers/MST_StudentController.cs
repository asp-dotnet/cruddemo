﻿using CRUDDemo.Areas.Branch.Models;
using CRUDDemo.Areas.City.Models;
using CRUDDemo.Areas.Student.Models;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using System.Data.SqlClient;

namespace CRUDDemo.Areas.Student.Controllers
{
    [Area("Student")]
    [Route("Student/[controller]/[action]")]

    public class MST_StudentController : Controller
    {
        private IConfiguration Configuration;
        public MST_StudentController(IConfiguration configuration)
        {
            Configuration = configuration;
        }


        #region SelectAll
        [Route("")]
        [Route("StudentList")]
        public IActionResult Index()
        {
            DataTable dt = new DataTable();
            string str = Configuration.GetConnectionString("myConnectionString");
            SqlConnection conn = new SqlConnection(str);
            conn.Open();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "PR_Mst_Student_SelectAll";
            SqlDataReader reader = cmd.ExecuteReader();

            if (reader.HasRows)
            {
                dt.Load(reader);
            }
            return View("MST_StudentList", dt);
        }
        #endregion

        #region SelectByPK

        public IActionResult Add(int? StudentID)
        {
            FillCityDDL();
            FillBranchDDL();
            if (StudentID != null)
            {
                SqlConnection objConn = new SqlConnection(this.Configuration.GetConnectionString("myConnectionString"));
                objConn.Open();
                SqlCommand objCmd = objConn.CreateCommand();
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.CommandText = "PR_Mst_Student_SelectByPK";
                objCmd.Parameters.AddWithValue("@StudentID", StudentID);

                SqlDataReader objSDR = objCmd.ExecuteReader();

                MST_StudentModel student = new MST_StudentModel();

                if (objSDR.HasRows)
                {
                    while (objSDR.Read())
                    {
                        student.StudentName = objSDR["StudentName"].ToString();
                        student.BranchID = Convert.ToInt32(objSDR["BranchID"]);
                        student.CityID = Convert.ToInt32(objSDR["CityID"]);
                        student.MobileNoStudent = objSDR["MobileNoStudent"].ToString();
                        student.Email = objSDR["Email"].ToString();
                        student.MobileNoFather = objSDR["MobileNoFather"].ToString();
                        student.Address = objSDR["Address"].ToString();
                        student.BirthDate = Convert.ToDateTime(objSDR["BirthDate"]);
                        student.Age = Convert.ToInt32(objSDR["Age"]);
                        student.IsActive = Convert.ToBoolean(objSDR["IsActive"]);
                        student.Gender = objSDR["Gender"].ToString();
                        student.Password = objSDR["Password"].ToString();
                    }
                }
                return View("MST_StudentAddEdit", student);
            }
            return View("MST_StudentAddEdit");
        }
        #endregion

        #region ADD EDIT
        [HttpPost]
        public IActionResult Save(MST_StudentModel modelMST_Student)
        {
            if (ModelState.IsValid)
            {

                SqlConnection objConn = new
                SqlConnection(this.Configuration.GetConnectionString("myConnectionString"));
                objConn.Open();
                SqlCommand objCmd = objConn.CreateCommand();
                objCmd.CommandType = CommandType.StoredProcedure;
                if (modelMST_Student.StudentID == null)
                {
                    objCmd.CommandText = "PR_Mst_Student_Insert";
                    objCmd.Parameters.AddWithValue("@Created", DateTimeOffset.Now);
                }
                else
                {
                    objCmd.CommandText = "PR_Mst_Student_UpdateByPK";
                    objCmd.Parameters.AddWithValue("@StudentID", modelMST_Student.StudentID);
                }

                objCmd.Parameters.AddWithValue("@StudentName", modelMST_Student.StudentName);
                objCmd.Parameters.AddWithValue("@BranchID", modelMST_Student.BranchID);
                objCmd.Parameters.AddWithValue("@CityID", modelMST_Student.CityID);
                objCmd.Parameters.AddWithValue("@MobileNoStudent", modelMST_Student.MobileNoStudent);
                objCmd.Parameters.AddWithValue("@Email", modelMST_Student.Email);
                objCmd.Parameters.AddWithValue("@MobileNoFather", modelMST_Student.MobileNoFather);
                objCmd.Parameters.AddWithValue("@Address", modelMST_Student.Address);
                objCmd.Parameters.AddWithValue("@BirthDate", modelMST_Student.BirthDate);
                objCmd.Parameters.AddWithValue("@Age", modelMST_Student.Age);
                objCmd.Parameters.AddWithValue("@IsActive", modelMST_Student.IsActive);
                objCmd.Parameters.AddWithValue("@Gender", modelMST_Student.Gender);
                objCmd.Parameters.AddWithValue("@Password", modelMST_Student.Password);




                if (Convert.ToBoolean(objCmd.ExecuteNonQuery()))
                {
                    if (modelMST_Student.StudentID == null)
                        TempData["Message"] = " ";
                    else
                    {
                        TempData["Message"] = " ";
                        return RedirectToAction("Index");
                    }
                }
            }
            return RedirectToAction("Index");
        }


        #endregion


        #region DELETE
        [Route("Delete")]
        public IActionResult DeleteStudent(int StudentID)
        {
            Console.Write(StudentID);
            SqlConnection objConn = new SqlConnection(this.Configuration.GetConnectionString("myConnectionString"));
            objConn.Open();
            SqlCommand objCmd = objConn.CreateCommand();
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.CommandText = "PR_Mst_Student_DeleteByPK ";
            objCmd.Parameters.Add("@StudentID", SqlDbType.Int).Value = StudentID;
            if (Convert.ToBoolean(objCmd.ExecuteNonQuery()))
            {
                TempData["Message"] = "Delete Successfully ";
            }
            objConn.Close();
            return RedirectToAction("Index");
        }

        #endregion

        #region Fill City DropDown
        public void FillCityDDL()
        {
            List<CityDropDownModel> list = new List<CityDropDownModel>();
            SqlConnection con = new SqlConnection(this.Configuration.GetConnectionString("myConnectionString"));
            con.Open();
            SqlCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "PR_City_SelectAll";
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    CityDropDownModel cityDropDown = new CityDropDownModel()
                    {
                        CityID = Convert.ToInt32(dr["CityID"]),
                        CityName = dr["CityName"].ToString()
                    };
                    list.Add(cityDropDown);
                }
                dr.Close();
            }
            con.Close();
            ViewBag.CityList = list;
        }
        #endregion

        #region Fill Branch DropDown
        public void FillBranchDDL()
        {
            List<BranchDropDownModel> list = new List<BranchDropDownModel>();
            SqlConnection con = new SqlConnection(this.Configuration.GetConnectionString("myConnectionString"));
            con.Open();
            SqlCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "PR_Mst_Branch_SelectAll";
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    BranchDropDownModel brancheDropDown = new BranchDropDownModel()
                    {
                        BranchID = Convert.ToInt32(dr["BranchID"]),
                        BranchName = dr["BranchName"].ToString()
                    };
                    list.Add(brancheDropDown);
                }
                dr.Close();
            }
            con.Close();
            ViewBag.BranchList = list;
        }
        #endregion

    }
}
