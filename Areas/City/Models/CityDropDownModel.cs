﻿namespace CRUDDemo.Areas.City.Models
{
    public class CityDropDownModel
    {
        public int? CityID { get; set; }
        public string? CityName { get; set; }
    }
}
