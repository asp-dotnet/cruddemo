﻿using CRUDDemo.Areas.City.Models;
using CRUDDemo.Areas.Country.Models;
using CRUDDemo.Areas.State.Models;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using System.Data.SqlClient;

namespace CRUDDemo.Areas.City.Controllers
{
    [Area("City")]
    [Route("City/[controller]/[action]")]
    public class LOC_CityController : Controller
    {
        private IConfiguration Configuration;
        public LOC_CityController(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        #region SelectAll
        public IActionResult Index()
        {
            DataTable dt = new DataTable();
            string str = Configuration.GetConnectionString("myConnectionString");
            SqlConnection conn = new SqlConnection(str);
            conn.Open();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "PR_City_SelectAll";
            SqlDataReader dr = cmd.ExecuteReader();

            if (dr.HasRows)
            {
                dt.Load(dr);
            }
            return View("LOC_CityList", dt);
        }
        #endregion

        #region SelectByPK
        public IActionResult Add(int? CityID)
        {
            FillCountryDDL();
            FillStateDDL();

            if (CityID != null)
            {
                String str = Configuration.GetConnectionString("myConnectionString");
                SqlConnection conn = new SqlConnection(str);
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "PR_City_SelectByPK";

                cmd.Parameters.Add("@CityID", SqlDbType.Int).Value = CityID;
                DataTable dt = new DataTable();
                SqlDataReader reader = cmd.ExecuteReader();
                LOC_CityModel city = new LOC_CityModel();
                if (reader.Read())
                {
                    city.CityName = reader["CityName"].ToString();
                    city.CityCode = reader["CityCode"].ToString();
                    city.StateID = Convert.ToInt32(reader["StateID"]);
                    city.CountryID = Convert.ToInt32(reader["CountryID"]);

                }
                return View("LOC_CityAddEdit", city);
            }
            return View("LOC_CityAddEdit");
        }
        #endregion

        #region Add Edit

        [HttpPost]
        public IActionResult Save(LOC_CityModel modelLOC_City)
        {
            if (ModelState.IsValid)
            {
                string str = Configuration.GetConnectionString("myConnectionString");
                SqlConnection conn = new SqlConnection(str);
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;

                if (modelLOC_City.CityID == null)
                {
                    cmd.CommandText = "PR_City_Insert";
                    cmd.Parameters.AddWithValue("@CreationDate", DateTimeOffset.Now);
                }
                else
                {
                    cmd.CommandText = "PR_City_UpdateByPK";
                    cmd.Parameters.Add("@CityID", SqlDbType.Int).Value = modelLOC_City.CityID;
                }
                cmd.Parameters.Add("@CityName", SqlDbType.VarChar).Value = modelLOC_City.CityName;
                cmd.Parameters.Add("@CityCode", SqlDbType.VarChar).Value = modelLOC_City.CityCode;
                cmd.Parameters.Add("@StateID", SqlDbType.Int).Value = modelLOC_City.StateID;
                cmd.Parameters.Add("@CountryID", SqlDbType.Int).Value = modelLOC_City.CountryID;

                if (Convert.ToBoolean(cmd.ExecuteNonQuery()))
                {
                    if (modelLOC_City.CityID == null)

                        TempData["CityInsertMsg"] = " ";
                    else
                    {
                        TempData["CityInsertMsg"] = "";
                        return RedirectToAction("Index");
                    }
                }
                conn.Close();
            }

            return RedirectToAction("Index");
        }
        #endregion

        #region Delete
        [Route("DeleteCountry")]
        public IActionResult Delete(int CityID)
        {
            SqlConnection conn = new SqlConnection(this.Configuration.GetConnectionString("myConnectionString"));
            conn.Open();
            SqlCommand sqlcommand = conn.CreateCommand();
            sqlcommand.CommandType = CommandType.StoredProcedure;
            sqlcommand.CommandText = "PR_City_DeleteByPK";
            sqlcommand.Parameters.Add("@CityID", SqlDbType.Int).Value = CityID;
            if (Convert.ToBoolean(sqlcommand.ExecuteNonQuery()))
            {
                TempData["Message"] = " ";
            }
            conn.Close();
            return RedirectToAction("Index");
        }
        #endregion

        #region FilStateDDL
        public void FillStateDDL()
        {
            List<StateDropDownModel> list = new List<StateDropDownModel>();
            SqlConnection con = new SqlConnection(this.Configuration.GetConnectionString("myConnectionString"));
            con.Open();
            SqlCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "PR_State_SelectAll";
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    StateDropDownModel state = new StateDropDownModel()
                    {
                        StateID = Convert.ToInt32(dr["StateID"]),
                        StateName = dr["StateName"].ToString()
                    };
                    list.Add(state);
                }
                dr.Close();
            }
            con.Close();
            ViewBag.StateList = list;
        }
        #endregion

        #region FillCountryDDL
        public void FillCountryDDL()
        {
            List<LOC_CountryDropDownModel> list = new List<LOC_CountryDropDownModel>();
            SqlConnection con = new SqlConnection(this.Configuration.GetConnectionString("myConnectionString"));
            con.Open();
            SqlCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "PR_Country_SelectAll";
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    LOC_CountryDropDownModel country = new LOC_CountryDropDownModel()
                    {
                        CountryID = Convert.ToInt32(dr["CountryID"]),
                        CountryName = dr["CountryName"].ToString()
                    };
                    list.Add(country);
                }
                dr.Close();
            }
            con.Close();
            ViewBag.CountryList = list;
        }
        #endregion

    }
}
