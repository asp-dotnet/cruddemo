﻿namespace CRUDDemo.Models
{
    public class CountryModel
    {
        public int? CountryID { get; set; }

        public string? CountryName { get; set; }

        public string? CountryCode { get; set; }

        public DateTime Created { get; set; }

        public DateTime Modified { get; set; }

    }
}
